/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');

require('./rankings');
require('./mixins');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('BoxPlotCard', require('./components/BoxPlotCard.vue'));
Vue.component('Podium', require('./components/Podium.vue'));
Vue.component('WeekCalendar', require('./components/WeekCalendar.vue'));

const app = new Vue({
    el: '#app',
});
