$(document).ready(function() {
    $('#ranking-tabs ul li').each(function(index, tab) {
        // console.log($(tab).text());
        $(tab).click(function(e) {
            e.preventDefault();
            // Hide currently active tab;
            var activeTable = $('#ranking-tabs ul li.is-active')
                .text()
                .trim();
            $('#' + activeTable).hide();
            $('#' + activeTable + '-title').hide();
            $('#' + activeTable + '-podium').hide();
            $('#ranking-tabs ul li.is-active').removeClass('is-active');

            $(this).addClass('is-active');
            activeTable = $(this)
                .text()
                .trim();
            $('#' + activeTable).show();
            $('#' + activeTable + '-title').show();
            $('#' + activeTable + '-podium').show();
        });
    });
});
