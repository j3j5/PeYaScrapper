<!DOCTYPE html>
<!--

                                                    ..,;:cc:;..
                                                 ,oO000O0000000Oo;
                                              .oO000000O00000000000l.
                                             :OOOO0OOOOOOO0OOOOOO0OOO;
                                            c0OOOO0OOOOOOO0OOOOOO0OOOOc                                            .;l,;.
                                           .0000O000000O000000000000000d                                       .;oxk0xd;,
                                           d0000O000000O000000000000000O'                                 .':okkO0k00k;:
                                       .',cO0000O000000O000000000000000O,                             .,oxkKO0KO0Kkxl::
                                   .:oOOkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOk.                         .;lkxO0xO0xOOlc,';lo,
                               'cdO00000O0000000O000000O00000000000000Oc                     .'cdk0OOKO0KOko::;:oooooo
                            ,o000O000000O0000000O000000O0000000000000Ol                  .,lkkOKOO0OO0dcc:;cododdoodo.
                         'oO00000O000000O0000000O000000O00000000000Ox,               .;lxOkk0kk0kkdc;,codoooodoodooo:
                       :kOOOOO0OOOOOOOxdOOO0OOOOOdOOOOOOOO0OOOOOOOd,.           .'cdkOK0OK0OKkll;;clloooooolooooooll
                    .lO0OO00000OOO000O0;l0O000OOOdccoxOOO00OOOxl;.          .,ldOOk0Ok0Okxc;;,cdodoooododoooodooooo'
                  .l00000O0000000O00000k'0000000O000kol;,'...          ...cod0Ok0Ox0koc;;:ddodddddddoddddddodddodd:
                 cO000000O0000000O00000x.0000000O000000k;              o0c'dlO0OKO:l;codoodoooododoooododoooodl:;l
               'kOOOO0O0OOOO0O0OOOO0OO0c;OO0O0OOOO0OOOOOk,            :OOk.oldOk0o.oolooooooloooooooloooooolooO.l.
              :O0OOOO0O0OOOOOO0OOOO0OO0'oOO0O0OOOO0OOOOOOk:          'd0Ox.ockOk0.loolooooooloooooooloooooooxO:;,
            .d0000O000000O0000000O00000,O0000000O000000O000d'       .xOK0;:odK0Od'doooodoodoooododoooododod0Ok.l
           .k00000O000000O0000000O0000x;O0000000O000000O0000Ox;.    :Ox0l.dox0Oo.ddddodddddoldddddddoddoldk0l'o.
          .kOOOO0OOOO0O0OOOO0O0OOOO0OOx;OOO0O0OOOO0OkkOOOO0OOOOOo;.,k0kO.llOk0k.loooollooddkkcoooooolool0kO:'o:
         .kOOOOO0OOOO0O0OOOO0O0OOOO0OOO'kOO0O0OOxoloxdcoOO0OOOOOO0ddK0k;;dx0OK;'oodoxxOOOK0OKoloooooodx0Ko,;oo
         d0O000000O000000O0000000O00000::000xllokXNNNNNOlcdO000000dxOO:'doOOx0llkkx0Ox0kk0kk0xoddddodOxk:,ood,
        :00O000000O000000O0000000O00000O,:ookXNNNNNNNNNNNXxlolldkd0k0x'odk00OK0OK0O00xdccokOKOkloolkO0l,loooc
       .O00O00O000O000000O000000OO000000OxKNNNNNNNNNNKKKKXNNXKOxox0k0.loOO00OK0OOocl::looolxKOOxoo0Ok;;odoo:
       :OOOOOOOOOOOOOOOOOOOOOOOxxkOOOOOOOOx0NNNNXK0KKNNNKKKKXNX0d0Ox::cdOxOOol,,;:ollclollocOkk0kkO:.coloc'
       k000O000000O000000OOdlloxl:O00000O00kk0KKKNNXKKKKXNXKKKKdkK0o'oo00O0';loododoooodoodldOOKOo;:ool,.
      .0000O000000O000xllodONNNNN0:lO000O0000xkKKKKXNNKKKKXNNKx0k0k.olx00k;cdodddddddodddoddlo:c;,,'.
      '0000O00000OocllxXNNNNNNNNNNNd;l00O00000OkO0KKKKNNXKKKX0o0x0'llkx0Ol'doooododoooodoodol;'.
      ,O0OOOOOOxcoOXNNNNNNNNNNXKKKKXNk:cdOO0OOOOOkkk0O0KXNX0kd0KOo'doKOKO.loolooooooolooc;.
      ;00OxdkkodooxxkxOKKXXKKKKNNXKKKKNXdccdO000O000OkxxkkxOdxO0l.ooOOx0.:odoooododl:..
      '00ldllx'OkdxdddooddodxOOkk0NNKKKKXNXxcolok000000O000xkk0O.ldd00xc'ddddooc,.
      .00oxkO0d'lkK0OK0kOxxxdddolooodkO0Ok0XNXKkdooocloxO0OoKO0,:dkOKO:,ooc;.
       k0Ool:'.   ...;:cdxx0kk0kkOxxxolooollddxkOOddolc::;ck0xc;lcc;:','.
       ;:.                  ..;:cddx0OO0kOOxxxdddooooooooxx00c,ll:,.
                                     ..,,:lox0O0KOOOxkkx0KOx;::.
                                                ..,:coxlo:,:o.
                                                       .';,.
 -->
<html lang="{{ config('app.locale') }}" moznomarginboxes mozdisallowselectionprint>
    @include('partials._head')
    <body class="{{$body_class or ""}}">
        @include('partials._ribbon')
        <div id="app" class="container-fluid">
            @include('partials._nav')
            @include('partials._hero')
            <section class="section">
                @yield('content')
            </section>
            @include('partials._footer')
        </div>

        <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
        </script>
        <script src="/js/app.js"></script>
    </body>
</html>
