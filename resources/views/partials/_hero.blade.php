<section class="hero is-info is-bold">
    <div class="container">
        <nav class="breadcrumb is-large" aria-label="breadcrumbs">
            <ul>
                <li><a style="padding-left:2px;" href="/"><span class="icon is-small"><i class="fa fa-home"></i></span><span>Home</span></a></li>
                @if($country)
                <li><a href="{{route('getCountry', [$country])}}"><span class="icon is-small"><i class="fa fa-flag"></i></span><span>{{$country->name}}</span></a></li>
                @endif
                @isset($city)
                <li><a href="/{{route('getCity', [$country, $city])}}"><span class="icon is-small"><i class="fa fa-map-marker"></i></span><span>{{$city->name}}</span></a></li>
                @endisset
            </ul>
        </nav>
    </div>
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                {!! $title !!}
            </h1>
            <h2 class="subtitle">
                {!! $description or '' !!}
            </h2>
        </div>
    </div>
</section>
