
<!-- BBro -->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//{{$piwik_base_url}}/";
    _paq.push(['setTrackerUrl', u+'bbro.php']);
    _paq.push(['setSiteId', '{{$piwik_site_id}}']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'bbro.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//bbro.dezwartepoet.nl/bbro.php?idsite=16&rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End BBro Code -->
