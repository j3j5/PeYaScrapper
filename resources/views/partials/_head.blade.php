<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @php
    $foods = ['🍔', '🍟', '🍝', '🍛', '🍣', ' 🍺', '🍜', '🍲', ];
    $html_title = $foods[mt_rand(0, count($foods)-1)] . ' ' ;
    if(isset($title) && !empty($title)) {
         $html_title .= strip_tags($title) . ' | ';
    } else {
        $html_title .= ' Food Data Analytics ';
    }
    $html_title .= " PY Montevideo Stats " . $foods[mt_rand(0, count($foods)-1)];

    $default_description = "Un análisis de restaurantes y precios en una conocida plataforma de comida a domicilio.";
    @endphp
    <title> {{ $html_title }}</title>

    <!-- COMMON TAGS -->
    <!-- Search Engine -->
    <meta name="description" content="{{$description or $default_description}}">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="{{ $html_title }}">
    <meta itemprop="description" content="{{$description or $default_description}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ $html_title }}">
    <meta name="twitter:description" content="{{$description or $default_description}}">
    <meta name="twitter:site" content="@julioelpoeta">
    <meta name="twitter:creator" content="@julioelpoeta">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ $html_title }}">
    <meta name="og:description" content="{{$description or $default_description}}">
    <meta name="og:url" content="{{config('app.url')}}">
    <meta name="og:site_name" content="PY @if(isset($city)) {{ $city->name }} @if(isset($country)) ({{$country->name}}) @endif @else @if(isset($country)) {{$country->name}} @endif @endif Stats">
    <meta name="og:locale" content="es_UY">
    <meta name="og:type" content="website">

    <link rel="stylesheet" href="{{secure_asset('css/app.css')}}">
    @if (app()->environment('production'))
        @include('partials._analytics')
    @endif
</head>
