<nav class="navbar has-shadow">
    {{-- <div class="container"> --}}
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <i class="fa fa-lg fa-line-chart nav-logo" aria-hidden="true"></i>PeYa Stats
            </a>

            <button class="button navbar-burger" data-target="navMenu">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>

        <div id="navMenu" class="navbar-menu">
            <div class="navbar-start">
                @foreach($navigation_items as $name => $url)
                <a class="navbar-item is-tab @if(request()->is( substr(parse_url($url)['path'], 1) )) is-active @endif" href="{{$url}}">
                    {{$name}}
                </a>
                @endforeach
            </div>
            <div class="navbar-end">
            </div>
        </div>
    {{-- </div> --}}
</nav>
