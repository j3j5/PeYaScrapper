<footer class="footer is-link">
    <div class="container has-text-centered">
        <span class="is-size-5">
            Made with
            @foreach($madewith as $icon)
                <i class="fa footer-icons fa-{{$icon}}" aria-hidden="true"></i> @if(!$loop->last) &amp; @endif
            @endforeach
            by <a href="https://gitlab.com/j3j5" target="_blank" rel="noopener" title="j3j5">j3j5</a>
                <i class="fa fa-long-arrow-right footer-arrow" aria-hidden="true"></i>
                <span title="Algunos derechos reservados">
                    <i class="fa fa-copyright footer-copyright" aria-hidden="true"></i>
                </span> 2017 @if(date('Y') !== "2017") - {{ date('Y') }} @endif
        </span>
    </div>
</footer>
