@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="columns is-multiline">
        @foreach($podiums as $title=> $restaurants)
        <div class="column is-one-quarter-desktop is-6-tablet">
            <podium title="{{$title}}" :champions="{{$restaurants}}"></podium>
        </div>
        @endforeach
    </div>
</div>
@endsection
