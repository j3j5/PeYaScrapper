<table id="{{ $tab['name'] }}" class="table is-narrow ranking-table" @if(!$loop->first)style="display: none;"@endif>
    <thead>
        <tr>
            <th><abbr title="Posición">#</abbr></th>
            <th>Restaurante</th>
            <th>Precio medio</th>
            <th>Producto/Plato</th>
            <th>Precio</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th><abbr title="Posición">Pos</abbr></th>
            <th>Restaurante</th>
            <th>Precio medio</th>
            <th>Producto/Plato</th>
            <th>Precio</th>
        </tr>
    </tfoot>
    <tbody>
    @foreach($rankings[$tab['name']] as $category => $restaurants)
        @foreach($restaurants as $position => $restaurant)
        <tr class="restaurant-header">
            <th>@if($category=='cheap'){{ ($position + 1) }}@else {{$position}} @endif</th>
            <th title="{{$restaurant->area->name}}">{{ $restaurant->name }} @if(is_null($city)) ({{$restaurant->city->name}}) @endif</th>
            <td>{{ $restaurant->avg_price }}</td>
            <td colspan=2></td>
        </tr>
            @foreach($restaurant->category_dishes as $dish)
        <tr class="dish-row">
            <td colspan="3"></td>
            <td>{{ $dish->name }} @if($dish->description) <small> {{'(' . $dish->description . ')' }}</small>@endif </td>
            <td>{{ $dish->readable_price }}</td>
        </tr>
            @endforeach
        @endforeach
        @if(!$loop->last)
        <tr> <td colspan="6"></td> </tr>
        <tr class="has-text-centered">
            <td colspan="6" style=" text-align:center; ">...</td>
        </tr>
        <tr> <td colspan="6"></td> </tr>
        @endif
    @endforeach
    </tbody>
</table>
