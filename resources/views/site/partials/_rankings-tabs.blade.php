<div id="ranking-tabs" class="tabs is-large is-centered is-boxed tabs">
    <ul>
        @foreach($tabs as $tab)
        <li class="@if($loop->first) is-active @endif">
            <a>
            @if(isset($tab['icon']))
                @if (preg_match('/pe-is-f.*/', $tab['icon']))
                    <span class="icon"><i class="{{$tab['icon']}}"></i></span>
                @else
                    <span class="icon"><i class="fa {{$tab['icon']}}"></i></span>
                @endif
            @endif
                <span>{{ $tab['name'] }}</span>
            </a>
        </li>
        @endforeach
    </ul>
</div>
