@php
// dd($product, $data);
@endphp

<div class="card">
  <div class="card-content">
    <div class="media">
      <div class="media-left">
        <figure class="is-48x48">
            <span class="icon is-large is-size-2"><i class="{{App\Repositories\RankingRepository::getIconForGroup($product)}}"></i></span>
        </figure>
      </div>
      <div class="media-content">
        <p class="title is-4">{{$product}}</p>
      </div>
    </div>

    <div class="content">
      @foreach($data as $type => $text)
          <p>{!!$text['description']!!}</p>
      @endforeach
    </div>
  </div>
</div>
