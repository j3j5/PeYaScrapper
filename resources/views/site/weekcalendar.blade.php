@extends('layouts.master')

@section('content')
<div class="container">
    <week-calendar title="Horarios" :times={{$times}} :total={{$max_restaurants}} ></week-calendar>
</div>
@endsection
