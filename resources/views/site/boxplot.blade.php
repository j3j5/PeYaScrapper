@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="columns is-multiline is-desktop">
        @foreach($product_groups as $products)
            <div class="column is-half-desktop">
                <box-plot-card :country="{{$country}}" :city="{{$city or '{}'}}" title="{{ $products }}"></box-plot-card>
            </div>
        @endforeach
        </div>
    </div>
@endsection
