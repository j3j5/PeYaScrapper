@extends('layouts.master')

@section('content')

@include('site.partials._rankings-tabs')

<div class="container ranking-page">
    @foreach($tabs as $tab)
        <h2 id="{{ $tab['name'] . '-title' }}" class='title is-2 has-text-centered ranking-title' @if(!$loop->first) style="display:none;" @endif >
            <span class="icon is-large"><i class="{{$tab['icon']}}"></i></span>
            <span>{{ $tab['name'] }}</span>
            <span class="icon is-large"><i class="{{$tab['icon']}} pe-flip-horizontal"></i></span>
        </h2>

        <div id="{{ $tab['name'] . '-podium' }}" class="columns is-multiline" @if(!$loop->first) style="display:none;" @endif>

            @php
            $tab_elements = 0;
            foreach($rankings[$tab['name']] as $ranking) {
                $tab_elements += $ranking->count();
            }
            @endphp

            @foreach($podiums[$tab['name']] as $podium)

            @if($tab_elements >= 6)
            <div class="column @if($loop->first) is-offset-3 @endif is-3 ">
                <podium title=
                    @if($loop->first)
                        "LOS + BARATOS"
                    @else
                        "LOS + CAROS"
                    @endif
                    :champions="{{$podium}}"></podium>
            </div>
            @else
                @if($loop->first)
                <div class="column is-offset-4 is-4 ">
                    <podium title="LOS + BARATOS" :champions="{{$podium}}"></podium>
                </div>
                @endif
            @endif
            @endforeach
        </div>
        @include('site.partials._ranking-table')
    @endforeach
</div>
@endsection
