@extends('layouts.master')

@php $min_restaurants = 10; @endphp

@section('content')

<div class="tile is-ancestor">
    <div class="tile is-vertical">
        <div class="tile is-parent">
            <article class="tile is-child">
                <p class="title">Ciudades</p>
                <p class="subtitle">Todas las ciudades y la cantidad de restaurantes disponibles en cada una.</p>
                <div class="container">
                    <div class="field is-grouped is-grouped-multiline">
                    @foreach($country->cities as $available_city)
                        <div class="control">
                            <a class="tags has-addons" @if($available_city->restaurants_count >= $min_restaurants) href="{{route('getRankings', [$country, $available_city])}}" @else title="No hay suficientes restaurantes como para mostrar estadísticas." @endif>
                                <span class="tag is-dark is-medium is-uppercase has-text-weight-semibold">{{$available_city->name}}</span><span class="tag @if($available_city->restaurants_count >= $min_restaurants) is-success @else is-danger @endif is-medium">{{$available_city->restaurants_count}}</span>
                            </a>
                        </div>
                    @endforeach
                    </div>
                </div>
            </article>
        </div>
        <hr>
        <h2 class="title">#{{$country->country_code}}FunFoodFacts</h2>
        <div class="columns is-multiline is-desktop">
            @foreach ($food_facts as $product => $data)
            <div class="column is-one-quarter-desktop">
                @include('site.partials._fun-food-fact-card')
            </div>
            @endforeach
        </div>
        <hr>
        <div>
            @include('site.partials._chivito-index')
        </div>
    </div>
</div>

@endsection
