<?php

namespace Deployer;

require 'recipe/laravel.php';

// Configuration

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('repository', 'git@gitlab.com:j3j5/PeYaScrapper.git');
set('shared_dirs', ['node_modules', 'storage']);

// Servers
host('linode_server')
    ->hostname('173.230.150.126')
    ->port(213)
    ->user('j3j5')
    ->forwardAgent(true) // You can use identity key, ssh config, or username/password to auth on the server.
    ->stage('production')
    ->set('deploy_path', '/webservers/PeYaStats'); // Define the base path to deploy your project to.

set('default_stage', 'production');
set('writable_use_sudo', true);
set('http_group', 'www-data');
// Tasks
set('writable_mode', 'chgrp');

desc('Reload Apache service');
task('apache2:reload', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo service apache2 reload');
});
after('deploy:symlink', 'apache2:reload');

set('bin/yarn', function () {
    return run('which yarn');
});

task('yarn:install', function () {
    run('cd {{deploy_path}}/release; {{bin/yarn}} install');
})->desc('Execute yarn install');

task('assets:generate', function () {
    run('cd {{deploy_path}}/release; npm run production');
})->desc('Generating assets');

after('deploy:vendors', 'yarn:install');
after('artisan:config:cache', 'artisan:route:cache');
after('artisan:config:cache', 'assets:generate');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
after('deploy:writable', 'artisan:migrate');
