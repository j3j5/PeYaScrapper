<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('url');
            $table->text('logo');
            $table->text('address');
            $table->text('categories');
            $table->text('top_categories');
            $table->float('rating');
            $table->integer('rating_count')->unsigned();
            $table->string('delivery_time');
            $table->string('coordinates')->nullable();
            $table->text('schedules')->nullable();
            $table->integer('area_id')->unsigned();

            $table->foreign('area_id')->references('id')->on('areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
