<?php

return [
    'base_url' => env('PIWIK_BASE_URL', ''),
    'site_id' => env('PIWIK_SITE_ID', ''),
];
