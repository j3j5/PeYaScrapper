<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');

Route::prefix('/{country}')->group(function () {
    Route::get('/', 'HomeController@country')->name('getCountry');

    Route::get('/rankings/{city?}', 'RankingController@index')->name('getRankings');
    Route::get('/boxplot/{city?}', 'BoxplotController@index')->name('getBoxplot');
    Route::get('/boxplot-data/{city?}', 'BoxplotController@boxplot')->name('getBoxplotApi');
    Route::get('/weekcalendar/{city?}', 'CalendarHeatmapController@index')->name('getCalendarHeatmap');

    Route::get('/{city}', 'HomeController@city')->name('getCity');
});
