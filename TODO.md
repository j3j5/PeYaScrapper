# TODO LIST

## Scrapper
* Get coordinates of restaurants.
* Get coordinates of delivery areas.

## Structure fails
* Restaurants don't belong to only one area.
** Change the relationship from 1-many to many-many.
* Restaurants don't have only one URL, they are accessible through more than one. Eg.- https://www.pedidosya.com.uy/restaurantes/pando/la-pasiva-pando-menu and https://www.pedidosya.com.uy/restaurantes/barros-blancos/la-pasiva-pando-menu

## Graphs
* Implement a function to order by median.
