<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $guarded = ['created_at', 'updated_at'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function restaurants()
    {
        return $this->hasMany(Restaurant::class);
    }

    public function country()
    {
        return $this->city->country();
    }

    public function scopeFromCity($query, City $city)
    {
        return $query->where('city_id', $city->id);
    }
}
