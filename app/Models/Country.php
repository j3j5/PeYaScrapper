<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $with = ['cities.areas'];

    protected $guarded = ['created_at', 'updated_at'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function areas()
    {
        return $this->hasManyThrough(Area::class, City::class);
    }
}
