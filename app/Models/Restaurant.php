<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurant extends Model
{
    use SoftDeletes;

    private $days_of_the_week = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'schedules' => 'array',
    ];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function city()
    {
        return $this->area->city();
    }

    public function country()
    {
        return $this->area->country();
    }

    public function dishes()
    {
        return $this->hasMany(Dish::class);
    }

    public function getOpeningHoursAttribute()
    {
        $opening = [];
        foreach ($this->schedules as $day_of_week => $opening_times) {
            $dow_key = array_search($day_of_week, $this->days_of_the_week);
            $opening[$dow_key] = [];
            foreach ($opening_times as $times) {
                $opens = Carbon::parse($times[0]);
                $closes = Carbon::parse($times[1]);
                if ($closes->lt($opens)) {
                    $closes->addDay();
                }
                while ($closes->gt($opens)) {
                    $opening[$dow_key][$opens->hour] = true;
                    $opens->addHour();
                }
            }
        }
        return $opening;
    }

    public function scopeFromArea($query, Area $area)
    {
        return $query->where('area_id', $area->id);
    }

    public function scopeFromCity($query, City $city)
    {
        $areas = $city->areas()->pluck('id')->toArray();
        return $query->whereIn('area_id', $areas);
    }

    public function scopeFromCountry($query, Country $country)
    {
        $areas = $country->areas()->pluck('areas.id')->toArray();
        return $query->whereIn('area_id', $areas);
    }

    public function scopeWithAvailableSchedules($query)
    {
        return $query->where('schedules', 'NOT LIKE', '[]');
    }
}
