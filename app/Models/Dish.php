<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Dish extends Model
{
    protected $guarded = [];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function getReadablePriceAttribute()
    {
        return '$' . $this->price;
    }

    public function scopeFromRestaurants($query, Collection $restaurants)
    {
        return $query->whereIn('restaurant_id', $restaurants->pluck('id')->toArray());
    }

    public function scopeFromArea($query, Area $area)
    {
        $restaurants = Restaurant::fromArea($area)->pluck('id')->toArray();
        return $query->whereIn('restaurant_id', $restaurants);
    }

    public function scopeFromCity($query, City $city)
    {
        $restaurants = Restaurant::fromCity($city)->pluck('id')->toArray();
        return $query->whereIn('restaurant_id', $restaurants);
    }

    public function scopeFromCountry($query, Country $country)
    {
        $restaurants = Restaurant::fromCountry($country)->pluck('id')->toArray();
        return $query->whereIn('restaurant_id', $restaurants);
    }
}
