<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->app->bind('RollingCurlX', function ($app) {
            return new \marcushat\RollingCurlX(5);
        });

        view()->share('piwik_base_url', config('piwik.base_url'));
        view()->share('piwik_site_id', config('piwik.site_id'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
