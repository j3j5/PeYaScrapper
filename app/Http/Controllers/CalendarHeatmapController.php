<?php

namespace App\Http\Controllers;

use App\Repositories\OpeningRepository;
use Illuminate\Http\Request;

class CalendarHeatmapController extends Controller
{
    public function index(Request $request, OpeningRepository $repo)
    {
        if ($this->city) {
            $repo->setCity($this->city);
        } else {
            $repo->setCountry($this->country);
        }

        $times = $repo->getOpeningTimes();
        $max_restaurants = $repo->getMaxOpenedRestaurants();

        $data = [
            'title' => 'Horarios de apertura',
            'description' => $this->getPageDescription(),
        ];
        $data = array_merge($data, compact('times', 'max_restaurants'));

        return view('site.weekcalendar', $data);
    }

    private function getPageDescription()
    {
        $description = 'Este <em>heatmap</em> muestra la cantidad de restaurantes o locales abiertos dependiendo ';
        $description .= 'del día y hora de la semana.<br>Se puede observar a simple vista la mayor disponibilidad ';
        $description .= 'para pedir comimda en el horario de la cena.';

        return $description;
    }
}
