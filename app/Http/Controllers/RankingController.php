<?php

namespace App\Http\Controllers;

use App\Repositories\RankingRepository;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    private $groups = [
        'Cervezas',
        'Chivitos',
        'Pizzetas',
        'Milanesas',
        'Hamburguesas',
        'Papas',
        'Asado',
    ];

    private $tabs;
    protected $repo;

    public function index(Request $request, RankingRepository $repo)
    {
        $this->repo = $repo;
        if ($this->city) {
            $this->repo->setCity($this->city);
        } else {
            $this->repo->setCountry($this->country);
        }

        $this->tabs = $this->getRankingTabs();
        $rankings = $this->getRankings();
        $podiums = $this->getPodiums();

        $data = [
            'title' => 'Rankings',
            'description' => $this->getPageDescription(),
            'tabs' => $this->tabs,
            'podiums' => $podiums,
            'rankings' => $rankings,
        ];

        return view('site.rankings', $data);
    }

    private function getPageDescription()
    {
        $description = 'A continuación, un pequeño ranking de los restaurantes más baratos/caros ';
        $description .= 'por categoría de comida.<br>En el podium podemos ver los ganadores, calculando el ';
        $description .= 'precio medio de entre los platos disponibles dentro de la categoría.<br>Abajo se';
        $description .= 'muestra una tabla con los platos incluidos de los 5 más caros y los 5 más baratos.<br>';
        $description .= 'En algunos casos, la diferencia de precios se podría explicar por diferencias en la ';
        $description .= 'cantidad y/o calidad de los platos servidos.<br>En otros, la diferencia de precio se hace ';
        $description .= 'más difícil de justificar.';

        return $description;
    }

    private function getRankings()
    {
        $rankings = [];
        // Min restaurants on a category to show a tab
        $min_restaurants = 6;
        foreach ($this->tabs as $key => $tab) {
            $count = $this->repo->getCountRestaurantsByGroup($tab['name']);
            if ($count <= $min_restaurants) {
                unset($this->tabs[$key]);
                continue;
            }
            $rankings[$tab['name']] = [
                'count' => $count,
            ];

            // Max items on the table (cheap+expensive)
            $max_items = 10;
            if ($rankings[$tab['name']]['count'] > $max_items) {
                $rankings[$tab['name']]['cheap'] = $this->repo->getCheapRestaurantsByGroup($tab['name'], (int) $max_items / 2);
                $rankings[$tab['name']]['expensive'] = $this->repo->getExpensiveRestaurantsByGroup($tab['name'], (int) $max_items / 2);
            } else {
                $rankings[$tab['name']]['cheap'] = $this->repo->getCheapRestaurantsByGroup($tab['name'], $max_items);
            }
        }

        // Transform the collections into ranking objects
        $count = 0;
        foreach ($rankings as $section => &$categories) {
            foreach ($categories as $category => &$ranking) {
                if ($category === 'count') {
                    $count = $ranking;
                    unset($categories[$category]);
                    continue;
                }
                $ranking = $this->repo->buildRankingObject($ranking, $section);
                if ($category === 'expensive') {
                    $ranking = $ranking->reverse();
                    $offset = $ranking->count();
                    $ranking = $ranking->mapWithKeys(function ($item, $key) use ($count, $offset) {
                        $key = $count - $key;
                        return [$key => $item];
                    });
                }
            }
            unset($ranking);
        }
        unset($categories);

        return $rankings;
    }

    private function getPodiums()
    {
        $podiums = [];
        foreach ($this->tabs as $tab) {
            $podiums[$tab['name']] = [
                'cheap' => $this->repo->getCheapRestaurantsByGroup($tab['name']),
                'expensive' => $this->repo->getExpensiveRestaurantsByGroup($tab['name']),
            ];
        }
        // Transform the collections into podium objects
        foreach ($podiums as $section => &$categories) {
            foreach ($categories as $category => &$podium) {
                $podium = $this->repo->buildPodiumObject($podium);
            }
        }
        unset($podium);

        return $podiums;
    }

    private function getRankingTabs()
    {
        $tabs = [];
        foreach ($this->repo->getProductGroupNames() as $group) {
            $tabs[] = [
                'name' => $group,
                'icon' => RankingRepository::getIconForGroup($group),
            ];
        }
        return $tabs;
    }
}
