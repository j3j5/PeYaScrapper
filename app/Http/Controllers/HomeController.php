<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Dish;
use App\Repositories\RankingRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $product_groups;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $default_country = Country::where('country_code', 'UY')->first();
        return redirect()->route('getCountry', [$default_country]);
    }

    public function country(Request $request, RankingRepository $repo)
    {
        $title = $this->country->name;
        $this->country->load(['cities' => function ($query) {
            $query->withCount('restaurants')->has('restaurants', '>', 0)->orderBy('restaurants_count', 'desc');
        }]);
        $description = "Elige una de las siguientes ciudades o disfruta los #{$this->country->country_code}FunFoodFacts";
        $repo->setCountry($this->country);

        $groups = ['Cervezas', 'Chivitos', 'Ensaladas', 'Panchos', 'Papas', 'Sandwiches', 'Milanesas', 'Pizzetas'];

        foreach ($groups as $group) {
            $cheap = $repo->getCheapDishFromGroup($group, 1)->first();
            $cheap_count = $repo->getDishCountByPrice($group, $cheap->price);

            $expensive = $repo->getExpensiveDishFromGroup($group, 1, ['restaurant.area'])->first();
            $expensive_count = $repo->getDishCountByPrice($group, $cheap->price);

            $food_facts[$group]['cheapest']['description'] = $this->getDescription($group, $cheap, 'cheap', $cheap_count - 1);
            $food_facts[$group]['expensive']['description'] = $this->getDescription($group, $expensive, 'expensive', $expensive_count - 1);
        }
        return view('site.country', compact('title', 'description', 'food_facts'));
    }

    private function getDescription(string $group, Dish $dish, string $type, int $count)
    {
        switch ($group) {
            case  'Cervezas':
                $cheap_desc = 'La cerveza de litro <strong>más barata</strong> del país ';
                $expensive_desc = 'La <strong>más cara</strong> ';
                break;
            case 'Chivitos':
            case 'Panchos':
            case 'Sandwiches':
                $cheap_desc = 'El ' . strtolower(substr($group, 0, -1)) . ' <strong>más barato</strong> del país ';
                $expensive_desc = 'El <strong>más caro</strong> ';
                break;
            case 'Papas':
                $cheap_desc = 'Las ' . strtolower(substr($group, 0, -1)) . ' <strong>más baratas</strong> del país ';
                $expensive_desc = 'Las <strong>más caras</strong> ';
                break;
            case 'Pizzetas':
            case 'Milanesas':
            case 'Ensaladas':
                $cheap_desc = 'La ' . strtolower(substr($group, 0, -1)) . ' <strong>más barata</strong> del país ';
                $expensive_desc = 'La <strong>más cara</strong> ';
                break;
            default:
                break;
        }
        if ($type === 'cheap') {
            return $cheap_desc . $this->getEndOfDescription($dish, $count);
        }
        return $expensive_desc . $this->getEndOfDescription($dish, $count);
    }

    private function getEndOfDescription(Dish $dish, int $count)
    {
        $area = $this->getAreaByRestaurant($dish->restaurant);
        $city = $this->getCityByRestaurant($dish->restaurant);
        $end = "(<em>{$dish->name}</em>) cuesta {$dish->readable_price} y está en " . data_get($area, 'name', '????') .
            " (<a href='" . route('getCity', [$this->country, $city]) . "'>" .
            data_get($city, 'name', '????') . "</a>), en el restaurante \"{$dish->restaurant->name}\"";

        if ($count > 0) {
            $end .= " ($count " . str_plural('restaurante', $count) . ' más ';
            if ($count == 1) {
                $end .= 'tiene';
            } else {
                $end .= 'tienen';
            }
            $end .= ' el mismo precio)';
        }

        return $end;
    }

    public function city(Request $request, Country $country, City $city)
    {
        return redirect()->route('getRankings', [$country, $city]);
    }
}
