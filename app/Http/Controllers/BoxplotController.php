<?php

namespace App\Http\Controllers;

use App\Repositories\BoxplotRepository;
use Illuminate\Http\Request;

class BoxplotController extends Controller
{
    public function index(Request $request, BoxplotRepository $repo)
    {
        if ($this->city) {
            $repo->setCity($this->city);
        } else {
            $repo->setCountry($this->country);
        }

        $data = [
            'title' => 'Estadísticas | Diagramas de Caja',
            'description' => $this->getPageDescription(),
            'product_groups' => $repo->getProductGroupNames(),
        ];
        return view('site.boxplot', $data);
    }

    public function boxplot(Request $request, BoxplotRepository $repo)
    {
        if (!$request->filled('products')) {
            abort(404);
        }

        if ($this->city) {
            $repo->setCity($this->city);
        } else {
            $repo->setCountry($this->country);
        }
        $dishes = $repo->getPricesForChart($request->input('products'));

        return $dishes;
    }

    private function getPageDescription()
    {
        $description = 'Para poder visualizar mejor la distribución estadística de precios en diferentes platos, se utiliza el ';
        $description .= 'llamado <em>diagrama de cajas</em> (o <a href="https://en.wikipedia.org/wiki/Box_plot" target="_blank" rel="noreferer noopener">boxplot</a> en inglés).';
        $description .= 'se han agrupado por categorías(chivitos, milanesas...). <br> Dentro de cada categoría hay diferentes platos en ';
        $description .= 'los que se puede observar cómo es la variación de precios, para un mismo plato en los diferentes restaurantes.<br>';
        $description .= 'En algunos casos, la diferencia de precio puede deberse a tamaño y/o calidad, pero en otros (como en el caso de las cervezas) ';
        $description .= 'la diferencia se haya simplemente en el criterio del comercio en cuestión.<br>Para ver la agrupación de diferentes variantes ';
        $description .= 'dentro un mismo plato (eg.- <em>chivito-comun</em> y <em>chivito-comun-al-pan</em> se usan como un mismo plato) se puede ';
        $description .= '<a href="https://gitlab.com/j3j5/PeYaScrapper/blob/master/app/Repositories/BaseRepository.php#L87" target="_blank" rel="noreferer noopener">';
        $description .= 'ver el código en GitLab</a>.';

        return $description;
    }
}
