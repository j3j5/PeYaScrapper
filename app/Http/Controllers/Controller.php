<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\Restaurant;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $country;
    protected $city;

    protected $all_cities;
    protected $all_areas;

    public function __construct()
    {
        // Retrieve Country and City
        $this->middleware(function ($request, $next) {
            $response = $this->bootstrap($request);
            if ($response instanceof Response) {
                return $response;
            }

            return $next($request);
        });

        // Footer icons
        $madewith = ['heart', 'keyboard-o', 'headphones', 'coffee'];
        shuffle($madewith);
        view()->share(['madewith' => $madewith]);
    }

    protected function bootstrap($request)
    {
        $parameters = $request->route()->parameters();
        if (isset($parameters['country'])) {
            $this->country = $parameters['country'];
            view()->share('country', $this->country);
            if (isset($parameters['city'])) {
                $this->city = $parameters['city'];
            }
            view()->share('city', $this->city);
        } else {
            $this->country = Country::find(1);
        }

        $this->all_cities = $this->country->cities;
        $this->all_areas = $this->country->cities->flatMap(function ($city) {
            return $city->areas;
        });

        // Nav bar items
        $route_params = [$this->country];
        if ($this->city) {
            $route_params = [$this->country, $this->city];
        }
        $navigation_items = [
            'Rankings' => route('getRankings', $route_params),
            'Precios' => route('getBoxplot', $route_params),
            'Horarios' => route('getCalendarHeatmap', $route_params),
        ];
        view()->share('navigation_items', $navigation_items);
    }

    protected function getCityByRestaurant(Restaurant $restaurant)
    {
        $area = $this->getAreaByRestaurant($restaurant);
        if ($area) {
            return $this->all_cities->where('id', $area->city_id)->first();
        }
        return;
    }

    protected function getAreaByRestaurant(Restaurant $restaurant)
    {
        return $this->all_areas->where('id', $restaurant->area_id)->first();
    }
}
