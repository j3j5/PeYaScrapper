<?php

namespace App\Repositories;

use App\Models\Restaurant;

class OpeningRepository extends BaseRepository
{
    private $times;

    public function __construct()
    {
        parent::__construct();

        //
        $this->times = [];
        for ($day = 0; $day < 7; $day++) {
            for ($hour = 0; $hour < 24; $hour++) {
                $this->times[$day][$hour] = 0;
            }
        }
    }

    public function getOpeningTimes()
    {
        $restaurants = $this->restaurants->filter(function ($restaurant) {
            return !empty($restaurant->schedules);
        });

        $restaurants->each(function ($restaurant) {
            $this->times = $this->addOpeningTimes($restaurant, $this->times);
        });

        return collect($this->times);
    }

    public function getMaxOpenedRestaurants()
    {
        return collect($this->times)->map(function ($time) {
            return max($time);
        })->max() + 1;
    }

    private function addOpeningTimes(Restaurant $restaurant, array $times)
    {
        foreach ($restaurant->opening_hours as $day_of_week => $opening_times) {
            foreach ($opening_times as $hour => $value) {
                $times[$day_of_week][$hour]++;
            }
        }
        return $times;
    }
}
