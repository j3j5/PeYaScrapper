<?php

namespace App\Repositories;

use App\Models\Dish;
use Illuminate\Support\Facades\Cache;

class BoxplotRepository extends BaseRepository
{
    private $dishes_cache_key;

    public function __construct()
    {
        parent::__construct();
        $this->dishes_cache_key = $this->box_dishes_cache_key;
    }

    /**
     * This is the place to filter out some products from the parent function.
     *
     * @param string $group
     *
     * @return Collection All the slugs for the given group
     */
    public function getGroupSlugs(string $group)
    {
        switch ($group) {
            case 'Empanadas':
                return collect(self::$product_groups[$group])->take(6);
            default:
                return parent::getGroupSlugs($group);
        }
    }

    public function getProductGroupNames()
    {
        $group_names = self::$product_groups->keys();

        return $group_names->filter(function ($name) {
            foreach ($this->getGroupSlugs($name) as $product) {
                $total = $this->getDishQueryForGroup($name, $product)->count();
            }
            return (bool) $total;
        });
    }

    /**
     * Get the prices for a boxplot chart of a given product groups.
     *
     * @param string $group The product group
     *
     * @return Collection
     */
    public function getPricesForChart(string $group)
    {
        return Cache::remember($this->dishes_cache_key . $group, $this->cache_duration, function () use ($group) {
            $dishes = collect([]);
            foreach ($this->getGroupSlugs($group) as $product) {
                $query = $this->getDishQueryForGroup($group, $product);
                // Get the prices array
                $prices = $query->get()->pluck('price')->toArray();
                $product_name = is_array($product) ? $product[0] : $product;
                if (!empty($prices)) {
                    // Add the sample size to the product_name
                    $product_name .= ' [' . count($prices) . ']';
                    $dishes->push([$product_name, $prices]);
                }
            }
            return $dishes;
        });
    }

    protected function getDishQueryForGroup(string $group, $product)
    {
        $query = Dish::select('price')->fromRestaurants($this->restaurants)->orderBy('price');
        if (is_array($product)) {
            $query->whereIn('slug', $product);
        } else {
            $query->where('slug', $product);
        }
        return $query;
    }
}
