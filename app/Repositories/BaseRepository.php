<?php

namespace App\Repositories;

use App\Models\City;
use App\Models\Country;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Cache;

class BaseRepository
{
    protected $country;
    protected $city;
    protected $restaurants;

    protected $cache_restaurants_key = 'rests_';
    protected $ranking_restaurants_cache_key = 'rankRest_';
    protected $ranking_dishes_cache_key = 'rankDish_';
    protected $category_dishes_cache_key = 'rankCatDish_';
    protected $box_dishes_cache_key = 'boxDish_';

    protected $cache_duration = 5;

    public static $product_groups;
    public static $section_groups;

    public function __construct()
    {
        self::getProductGroups();
    }

    public function setCity(City $city)
    {
        $this->city = $city;
        $this->updateCacheKeys();
        $this->restaurants = Cache::remember($this->cache_restaurants_key, $this->cache_duration, function () {
            return Restaurant::with('area')->fromCity($this->city)->get();
        });
        $this->filterProductGroups();
    }

    public function setCountry(Country $country)
    {
        $this->country = $country;
        $this->updateCacheKeys();
        $this->restaurants = Cache::remember($this->cache_restaurants_key, $this->cache_duration, function () {
            return Restaurant::with('area')->fromCountry($this->country)->get();
        });
        $this->filterProductGroups();
    }

    public function getProductGroupNames()
    {
        return self::$product_groups->keys();
    }

    public function getProductGroup(string $group)
    {
        return collect(self::$product_groups->get($group));
    }

    public function getSectionGroups()
    {
        return self::$section_groups;
    }

    public function getSectionGroup(string $group)
    {
        return collect(self::$section_groups->get($group));
    }

    public function getSectionGroupNames()
    {
        return self::$section_groups->keys();
    }

    /**
     * Get the product slugs for a given product.
     *
     * @param string $group
     *
     * @return Collection All the slugs for the given group.
     */
    public function getGroupSlugs(string $group)
    {
        return collect(self::$product_groups[$group]);
    }

    protected function filterProductGroups()
    {
        // TODO filter product groups if there are no restaurant on the city
    }

    protected function updateCacheKeys()
    {
        $add = '';
        if ($this->country) {
            $add .= '_' . $this->country->slug;
        }
        if ($this->city) {
            $add .= '_' . $this->city->slug;
        }
        $this->cache_restaurants_key .= $add;
        $this->ranking_restaurants_cache_key .= $add;
        $this->ranking_dishes_cache_key .= $add;
        $this->category_dishes_cache_key .= $add;
        $this->box_dishes_cache_key .= $add;
    }

    public static function getProductGroups()
    {
        if (!empty(self::$product_groups)) {
            return self::$product_groups;
        }
        self::$product_groups = collect([
            'Chivitos' => [
                [
                    'chivito-comun',
                    'chivito-comun-al-pan',
                ],
                [
                    'chivito-comun-con-fritas',
                    'chivito-comun-con-papas-fritas',
                    'chivito-de-carne-comun-con-papas-fritas',
                    'chivito-comun-con-guarnicion',
                    'chivito-comun-al-pan-con-papas-fritas',
                    'chivito-comun-al-pan-con-fritas',
                    'chivito-al-pan-comun-con-papas-fritas',
                    'chivito-al-pan-con-papas-fritas',
                ],
                'chivito-canadiense',
                'chivito-canadiense-con-papas-fritas',
                'chivito-canadiense-al-plato',
            ],
            'Milanesas' => [
                [
                    'milanesa-sola',
                    'milanesa',
                ],
                'milanesa-al-pan',
                [
                    'milanesa-en-2-panes',
                    'milanesa-en-dos-panes',
                ],
                [
                    'milanesa-al-plato-con-guarnicion',
                    'milanesa-con-guarnicion',
                    'milanesa-al-plato',
                ],
                [
                    'milanesa-napolitana',
                    'milanesa-a-la-napolitana',
                    'milanesa-napolitana-con-guarnicion',
                    'milanesa-napolitana-con-papas-fritas',
                ],
            ],
            'Pizzas' => [
                [
                    'faina',
                    'porcion-faina',
                    'faina-porcion',
                ],
                [
                    'pizza',
                    'pizza-comun',
                ],
                'figazza',
                [
                    'faina-con-muzzarella',
                    'porcion-faina-con-muzzarella',
                    'porcion-de-faina-con-muzzarella',
                ],
                'pizza-muzzarella',
            ],
            'Pizzetas' => [
                ['pizzeta-comun'],
                'pizzeta-con-muzzarella',
                'pizzeta-margarita',
                'pizzeta-capresse',
                'pizzeta-cuatro-quesos',
                'pizzeta-vegetariana',
                'pizzeta-napolitana',
                [
                    'pizzeta-tropical',
                    'pizzeta-hawaiana',
                ],
            ],
            'Papas' => [
                'pure-de-papas',
                'papa-al-plomo',
                [
                    'papas-fritas-porcion',
                    'papas-fritas',
                    'porcion-de-papas-fritas',
                    'porcion-papas-fritas',
                ],
                'papas-noisette',
                'papas-bravas',
                [
                    'papas-fritas-al-champignon',
                    'papas-champi',
                ],
                [
                    'gramajo',
                    'revuelto-de-gramajo',
                ],

            ],
            'Sandwiches' => [
                [
                    'sandwich-de-jamon-y-queso',
                    'sandwich-mixto',
                    'sandwich-cuadrado-de-jamon-y-queso',
                    'sandwich-jamon-y-queso',
                    'sandwich-frio-de-jamon-y-queso',
                    'sandwiche-de-jamon-y-queso',
                ],
                [
                    'sandwich-olimpico',
                    'sandwiche-olimpico',
                ],
                [
                    'sandwich-caliente',
                    'sandwiche-caliente',
                    'sandwich-caliente-comun',
                    'sandwich-caliente-de-jamon-y-queso',
                    'sandwich-caliente-clasico',
                ],
                [
                    'sandwich-napolitano',
                    'sandwiche-napolitano',
                ],
                [
                    'sandwich-caliente-con-muzzarella',
                    'sandwiche-caliente-con-muzzarella',
                    'sandwich-con-muzzarella',
                ],
                [
                    'sandwich-caliente-napolitano',
                    'sandwich-napolitano',
                ],
            ],
            'Ensaladas' => [
                'ensalada-de-frutas',
                'ensalada-mixta',
                'ensalada-rusa',
                'ensalada-de-pollo',
                'ensalada-de-atun',
                'ensalada-verde',
                'ensalada-capresse',
                'ensalada-cesar',
                'ensalada-griega',
                'ensalada-mediterranea',
            ],
            'Cervezas' => [
                [
                    'cerveza-patricia-lata',
                    'cerveza-patricia-330-ml',
                    'cerveza-patricia-en-lata',
                    'cerveza-patricia-300-ml',
                    'cerveza-patricia-330-cc',
                    'cerveza-patricia',
                ],
                [
                    'cerveza-pilsen-en-lata',
                    'cerveza-pilsen-330-ml',
                    'cerveza-pilsen-330-cc',
                    'cerveza-pilsen-de-330-ml',
                    'cerveza-pilsen-lata-354-ml',
                ],
                [
                    'pilsen-1-l',
                    'cerveza-pilsen-1-l',
                    'cerveza-pilsen-de-1-l',
                ],
                [
                    'patricia-1-l',
                    'cerveza-patricia-1-l',
                    'cerveza-patricia-de-1-l',
                    'cerveza-patricia-1l',
                ],
                [
                    'stella-artois-1-l',
                    'cerveza-stella-artois-1-l',
                    'cerveza-stella-artois-de-1-l',
                    'cerveza-stella-1-l',
                    'cerveza-stella-artrois-1l',
                    'cerveza-stella-artois-975-ml',
                ],
                [
                    'zillertal-1-l',
                    'cerveza-zillertal-1-l',
                    'cerveza-zillertal-de-1-l',
                    'cerveza-zillertall-1-l',
                    'cerveza-zillertal-l',
                ],
            ],
            'Refrescos' => [
                [
                    'refresco-500-ml',
                    'refresco-linea-pepsi-500-ml',
                    'refresco-pepsi-500-ml',
                ],
                [
                    'refresco-600-ml',
                    'refresco-600-cc',
                    'refresco-de-600-ml',
                    'refresco-coca-cola-600-ml',
                    'refresco-linea-coca-cola-600-ml',
                    'refresco-linea-coca-cola-600-cc',
                    'refresco-linea-coca-cola-de-600-ml',
                    'refrescos-linea-coca-cola-600-ml',
                    'gaseosa-linea-coca-cola-600-ml',
                ],
                [
                    'refresco-1-l',
                    'coca-cola-1-l',
                    'refresco-coca-cola-1-l',
                    'refresco-linea-coca-cola-1-l',
                    'refresco-linea-coca-cola-de-1-l',
                ],
                [
                    'refresco-1-5-l',
                    'refresco-linea-coca-cola-1-5-l',
                    'refresco-coca-cola-1-5-l',
                    'refresco-linea-pepsi-1-5-l',
                    'refresco-coca-cola-light-1-5-l',
                    'refresco-linea-coca-cola-de-1-5-l',
                    'refresco-pepsi-1-5-l',
                    'refresco-1-5-l-descartable',
                    'coca-cola-1-5-l',
                ],
            ],
            'Panchos' => [
                'pancho',
                'pancho-con-muzzarella',
                'pancho-con-panceta',
                'hungara-con-muzzarella',
                'hungara-con-panceta',
                'pancho-porteno',
            ],
            'Parrilla' => [
                'morcilla',
                'chorizo',
                'rinon',
                'chinchulin',
                'provolone',
                'molleja',
                [
                    'churrasco-de-cuadril',
                    'churrasco-de-cuadril-con-guarnicion',
                ],
                'asado',
                [
                    'entrecot',
                    'entrecot-con-guarnicion',
                ], // They are at the same range of prices, most likely they all include 'guarnición'
            ],
            'Hamburguesas' => [
                [
                    'hamburguesa-comun',
                    'hamburguesa-al-pan',
                    'hamburguesa-clasica',
                    'hamburguesa',
                    'hamburguesa-comun-al-pan',
                    'hamburguesa-casera-comun',
                    'hamburguesa-clasica-al-pan',
                ],
                [
                    'hamburguesa-con-papas-fritas',
                    'hamburguesa-al-pan-con-papas-fritas',
                    'hamburguesa-comun-con-papas-fritas',
                    'hamburguesa-comun-al-pan-con-papas-fritas',
                    'hamburguesa-al-pan-con-fritas',
                ],
                [
                    'hamburguesa-completa',
                    'hamburguesa-al-pan-completa',
                    'hamburguesa-completa-al-pan',
                ],
                [
                    'hamburguesa-completa-con-papas-fritas',
                    'hamburguesa-completa-al-pan-con-papas-fritas',
                    'hamburguesa-al-pan-completa-con-papas-fritas',
                    'hamburguesa-completa-al-pan-con-fritas',
                ],
            ],
            'Empanadas' => [
                'empanada-de-jamon-y-queso',
                [
                    'empanada-de-carne',
                    'empanada-de-carne-clasica',
                ],
                'empanada-de-pollo',
                [
                    'empanada-capresse',
                    'empanada-caprese',
                ],
                'empanada-de-queso-y-aceitunas',
                [
                    'empanada-de-queso-y-cebolla',
                    'empanada-de-cebolla-y-queso',
                ],
                'empanada-cuatro-quesos',
                'empanada-napolitana',
                'empanada-calabresa',
                'empanada-de-dulce-de-leche',
            ],
        ]);

        self::$section_groups = collect([
            'Chivitos' => [
                'Chivito',
                'Chivitos',
                'Los chivitos',
                'Chivitos al pan',
                'Chivitos de nalga',
                'Combo chivitos',
                'Chivitos y carnes',
                'Chivitos exquisitos',
                'Chivitos y panchos',
                'Nuestros súper chivitos',
                'Chivitos "rolls" tibios (1 persona)',
                'Chivitos (pan clásico o pan de pita)',
                // 'Promo chivitos',
                'Chivitos al plato',
                'Chivitos clásicos',
                'Chivitos especiales (nuevos)',
                'Chivitos regionales',
                'Nuestros chivitos',
                'Chivitos del juez',
                'Super chivitos',
                'Chivito de lomo',
                'Chivitos sureños',
                'Chivitos y burguers a las brasas',
                'Chivitos de lomo o pollo',
                'Chivitos (carne o pollo)',
                'Chivitos en pan ciabatta',
                'Chivitos Marcos lo arma a su gusto',
                'Chivitos gourmet',
                'Chivitos de carne o pollo',
                'Chivitos y hamburguesas',
                'Bocatas y chivitos con guarnición',
                'Chivitos vegetarianos de seitán',
                'Barbacoa charrúa',
                'Chivitos "La Mole" lo arma a su gusto',
            ],
        ]);
        return self::$product_groups;
    }

    public static function getIconForGroup(string $group)
    {
        $groups = self::getIconsForGroups();
        if (isset($groups[$group])) {
            return $groups[$group];
        }
        return 'pe-is-f-flatware-4';
    }

    public static function getIconsForGroups()
    {
        return [
            'Cervezas' => 'pe-is-f-beer-bottle',
            'Chivitos' => 'pe-is-f-burger-1',
            'Pizzetas' => 'pe-is-f-pizza-1',
            'Milanesas' => 'pe-is-f-steak-1-f',
            'Hamburguesas' => 'pe-is-f-burger-2',
            'Papas' => 'pe-is-f-french-fries',
            'Panchos' => 'pe-is-f-hot-dog',
            'Asado' => 'pe-is-f-barbecue-1',
            'Sandwiches' => 'pe-is-f-sandwich',
            'Ensaladas' => 'pe-is-f-tomato',
        ];
    }
}
