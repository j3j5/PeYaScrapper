<?php

namespace App\Repositories;

use App\Models\Dish;
use App\Models\Restaurant;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class RankingRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCheapRestaurantsByGroup(string $group, int $limit = 3)
    {
        return $this->getRestaurantsByGroup($group, 'cheap', $limit);
    }

    public function getExpensiveRestaurantsByGroup(string $group, int $limit = 3)
    {
        return $this->getRestaurantsByGroup($group, 'expensive', $limit);
    }

    public function getCheapDishFromGroup(string $group, int $limit = 1, array $with = [])
    {
        return $this->getDishFromGroup($group, 'cheap', $limit, $with);
    }

    public function getExpensiveDishFromGroup(string $group, int $limit = 1, array $with = [])
    {
        return $this->getDishFromGroup($group, 'expensive', $limit, $with);
    }

    public function getDishFromGroup(string $group, string $orderby, int $limit, array $with = [])
    {
        $query = $this->getQueryForDishByGroup();
        $query = $this->restrictQueryBasedOnGroup($group, $query);
        $query->take($limit);
        $query->with($with);

        switch ($orderby) {
            case 'expensive':
                $collection = Cache::remember($this->ranking_dishes_cache_key . "{$group}_{$orderby}_{$limit}", $this->cache_duration, function () use ($query) {
                    return $query->orderBy('price', 'DESC')->get();
                });
                break;
            case 'cheap':
                $collection = Cache::remember($this->ranking_dishes_cache_key . "{$group}_{$orderby}_{$limit}", $this->cache_duration, function () use ($query) {
                    return $query->orderBy('price', 'ASC')->get();
                });
                break;
            default:
                $collection = Cache::remember($this->ranking_dishes_cache_key . "{$group}_{$orderby}_{$limit}", $this->cache_duration, function () use ($query) {
                    return $query->get();
                });
                break;
        }

        return $collection;
    }

    /**
     * Get a collection of restaurants ordered by the average price of a given dish (or product-group/section).
     *
     * @param string $group The product group (or section) to filter the dishes.
     * @param string $orderby Whether to order the restaurants by cheapest or most expensive ones.
     * @param int $limit Amount of restaurants to be returned.
     *
     * @return
     */
    public function getRestaurantsByGroup(string $group, string $orderby, int $limit)
    {
        $query = $this->getQueryForRestaurantByGroup();
        $query = $this->restrictQueryBasedOnGroup($group, $query);
        $query->take($limit);

        switch ($orderby) {
            case 'expensive':
                $collection = Cache::remember($this->ranking_restaurants_cache_key . "{$group}_{$orderby}_{$limit}", $this->cache_duration, function () use ($query) {
                    return $query->orderBy('avg_price', 'DESC')->get();
                });
                break;
            case 'cheap':
                $collection = Cache::remember($this->ranking_restaurants_cache_key . "{$group}_{$orderby}_{$limit}", $this->cache_duration, function () use ($query) {
                    return $query->orderBy('avg_price', 'ASC')->get();
                });
                break;
            default:
                $collection = Cache::remember($this->ranking_restaurants_cache_key . "{$group}_{$orderby}_{$limit}", $this->cache_duration, function () use ($query) {
                    return $query->get();
                });
                break;
        }

        return $collection;
    }

    public function getCountRestaurantsByGroup(string $group)
    {
        return Cache::remember($this->ranking_dishes_cache_key . "{$group}_count", $this->cache_duration, function () use ($group) {
            $query = $this->getQueryForRestaurantByGroup();
            $query = $this->restrictQueryBasedOnGroup($group, $query);
            return $query->get()->count();
        });
    }

    public function getDishCountByPrice(string $group, int $price)
    {
        return Cache::remember($this->ranking_dishes_cache_key . "{$group}_{$price}_count", $this->cache_duration, function () use ($group, $price) {
            $query = $this->getQueryForDishByGroup();
            $query = $this->restrictQueryBasedOnGroup($group, $query);
            return $query->where('price', $price)->count();
        });
    }

    public function buildPodiumObject(Collection $collection)
    {
        return $collection->map(function ($item) {
            return [
                'title' => $item->restaurant->name,
                'description' => '$' . (int) $item->avg_price . ' de media',
                'image' => $item->restaurant->logo,
            ];
        });
    }

    public function buildRankingObject(Collection $collection, string $group)
    {
        $collection->transform(function ($item) use ($group) {
            $restaurant = $item->restaurant;
            $restaurant->category_dishes = $this->getCategoryDishesForRestaurant($restaurant, $group);
            $restaurant->avg_price = '$' . floor($item->avg_price);
            return $restaurant;
        });
        return $collection;
    }

    protected function getCategoryDishesForRestaurant(Restaurant $restaurant, string $group)
    {
        return Cache::remember($this->category_dishes_cache_key . "{$restaurant->id}_{$group}", $this->cache_duration, function () use ($restaurant, $group) {
            return $this->restrictQueryBasedOnGroup($group, $restaurant->dishes())->get();
        });
    }

    protected function getQueryForRestaurantByGroup()
    {
        return Dish::with('restaurant')
            ->select(DB::raw('*, AVG(`price`) AS avg_price'))
            ->fromRestaurants($this->restaurants)->groupBy('restaurant_id')
            ->havingRaw('COUNT(*) > 1');
    }

    protected function getQueryForDishByGroup()
    {
        return Dish::with('restaurant')->fromRestaurants($this->restaurants);
    }

    protected function restrictQueryBasedOnGroup(string $group, $query)
    {
        switch ($group) {
            case 'all':
                break;
            case 'Cervezas':
                // Remove the first two groups, beer cans
                $query->where(function ($query) use ($group) {
                    foreach ($this->getProductGroup($group)->slice(2) as $product_names) {
                        $query->orWhereIn('slug', $product_names);
                    }
                });
                break;
            case 'Chivitos':
                // Here use sections, the product groups don't include all different kind of Chivitos
                $query->where(function ($query) use ($group) {
                    $query->whereIn('section', $this->getSectionGroup($group)->toArray());
                    $query->where(function ($query) {
                        $query->where('slug', 'LIKE', '%chivito%');
                        $query->where('name', 'NOT LIKE', '%personas%');
                        $query->where('description', 'NOT LIKE', '%personas%');
                        $query->where('name', 'NOT LIKE', '%para dos%');
                        $query->where('name', 'NOT LIKE', '%para tres%');
                        $query->where('name', 'NOT LIKE', '%para 2%');
                    });
                });
                break;
            // case '':
            // SECTIONS
            // PRODUCT-GROUPS
            default:
                $query->where(function ($query) use ($group) {
                    foreach ($this->getProductGroup($group) as $product_names) {
                        if (is_array($product_names)) {
                            $query->orWhereIn('slug', $product_names);
                        } else {
                            $query->orWhere('slug', $product_names);
                        }
                    }
                });
                break;
        }
        return $query;
    }
}
