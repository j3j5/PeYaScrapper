<?php

namespace App\Console\Commands;

use App\Models\Area;
use App\Models\City;
use App\Models\Country;
use App\Models\Dish;
use App\Models\Restaurant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use marcushat\RollingCurlX;
use Sunra\PhpSimple\HtmlDomParser;

class PedidosYaScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:pedidosya
                            {--restaurants}
                            {--menus}
                            {--force-update}
                            {--restaurant=}
                            {--country=Uruguay}
                            {--city=Montevideo}
                            {--all-cities}
                            {--skip=0}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download all menu info from Pedidos YA';

    private $concurrent_reqs = 5;
    private $user_agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36';
    private $headers = [
        'Cookie' => '_pymkt=Campaign+Name%3DNot+set%26Campaign+Category%3DDirect%26Campaign+Source%3DDirect%26query%3Dpage%253D1%26creationDate%3D2017-07-18T06%3A06%3A06Z',
    ];

    private $country;
    private $city;

    // Get latest reviews
    // https://www.pedidosya.com.uy/review/getLastReviews?restaurantId=24414&bestReviews=true

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getCountry();
        if (!$this->option('all-cities')) {
            $cities = [$this->getCity($this->option('city'))];
        } else {
            $cities = $this->parseCities();
        }
        $skip = $this->option('skip') ?: 0;
        $skipped = 0;
        foreach ($cities as $city) {
            if ($skipped < $skip) {
                $skipped++;
                continue;
            }

            // Scrap restaurant info
            if ($this->option('restaurants')) {
                $url = $this->country->base_url . '/restaurantes/' . $city->slug;
                $info = [];

                $areas = $this->getAreasForCity($city);
                $start = false;
                foreach ($areas as $area) {
                    $restaurants = $this->getRestaurantsForArea($area);
                    $this->info($area->name . ' has ' . $restaurants->count() . ' restaurants.');
                }
            }

            // Scrap
            if ($this->option('menus')) {
                if ($this->option('restaurant')) {
                    $restaurants = Restaurant::where('name', 'LIKE', '%' . $this->option('restaurant') . '%')->fromCity($city)->get();
                } else {
                    if ($this->option('force-update')) {
                        $query = Restaurant::inRandomOrder();
                    } else {
                        $query = Restaurant::doesntHave('dishes');
                    }

                    $query->fromCity($city);

                    // $this->debug($query->toSql());
                    // $this->debug(json_encode($query->getBindings()));
                    $restaurants = $query->get();
                }
                $this->info($restaurants->count() . ' restaurants found.');

                if ($restaurants->isEmpty() || !$this->confirm('Continue?')) {
                    $this->info('Bye!');
                    continue;
                }
                $rolling = new RollingCurlX($this->concurrent_reqs);
                foreach ($restaurants as $restaurant) {
                    $rolling->addRequest($restaurant->url, [], function ($response, $url, $request_info, $user_data, $time) use ($restaurant) {
                        $dom = $this->cleanAndGetDom($response);
                        if (!$dom) {
                            $this->error('wtf!! ' . $url);
                            // Sometimes URLS change and PY redirects to the new URL
                            $new_url = $this->tryToFollowRedirect($url);
                            $this->debug($new_url);
                            $regex = "#https?://[.\w]+/restaurantes/[\w-]+/[\w-]+#";
                            if (strcmp($url, $new_url) !== 0 && preg_match($regex, $new_url)) {
                                $this->debug("Updating restaurant's URL from $url to $new_url");
                                $restaurant->update(['url' => $new_url]);
                            } elseif (is_int($new_url) && $new_url > 299) {
                                $this->debug("Deleting restaurant {$restaurant->name}.");
                                $restaurant->delete();
                            }
                            return;
                        }
                        $this->getMenuFromDom($dom, $restaurant);
                    }, null, [CURLOPT_USERAGENT => $this->user_agent], $this->headers);
                    //                 $this->getMenuForRestaurant($restaurant);
                }
                $rolling->execute();
                // Clean up
                $rolling = null;
            }
        }
    }

    private function getCountry()
    {
        $country = $this->option('country');
        $this->country = Country::where('name', $country)->orWhere('slug', $country)->first();
        if ($this->country) {
            return $this->country;
        }
        if (!$this->confirm("$country does not exist as a country on the DB. Do you want to create it?")) {
            $this->info('KTHXBYE');
            exit;
        }
        $country_code = $this->ask("What's the ISO country code for $country? Eg.- UY");
        $currency = $this->ask("What's the currency code for $country? Eg.- UYU");
        $base_url = $this->ask("What's the base URL for the website in $country? Eg.- https://pedidosya.com.uy for Uruguay");
        // Create
        $this->country = Country::create([
            'name' => $country,
            'slug' => str_slug($country),
            'country_code' => $country_code,
            'currency' => $currency,
            'base_url' => $base_url,
        ]);
        return $this->country;
    }

    private function getCity(string $city_name, $interactive = true)
    {
        $city = City::where('name', $city_name)->where('country_id', $this->country->id)->first();
        if ($city) {
            return $city;
        }
        if ($interactive && !$this->confirm("$city_name does not exist as a city of {$this->country->name} on the DB. Do you want to create it?")) {
            $this->info('KTHXBYE');
            exit;
        }
        $slug = str_slug($city_name);
        if ($interactive && !$this->confirm("Is $slug the slug for $city_name?")) {
            $slug = $this->ask('Write the slug then:');
        }

        $city = City::create([
            'name' => $city_name,
            'slug' => $slug,
            'country_id' => $this->country->id,
        ]);
        $this->debug($city->name . ' has been created.');
        return $city;
    }

    private function parseCities()
    {
        $homepage = $this->getDomFromUrl($this->country->base_url . '/restaurantes/');
        $json_cities = html_entity_decode($homepage->find('#citiesJSON')[0]->value);
        return collect(json_decode($json_cities, true))->transform(function ($city_info) {
            return $this->getCity($city_info['name'], false);
        });
    }

    private function getAreasForCity(City $city)
    {
        $this->debug('Retrieving areas');
        $homepage = $this->getDomFromUrl($this->country->base_url . '/restaurantes/' . $city->slug);
        $areas = collect([]);
        foreach ($homepage->find('#areasFilter li') as $li) {
            $name = html_entity_decode($li->find('a span', 0)->innertext);
            $areas->push(Area::firstOrCreate([
                'name' => $name,
                'slug' => str_slug($name),
                'city_id' => $city->id,
            ]));
        }
        $this->info($areas->count() . ' areas found and stored.');
        return $areas;
    }

    private function getRestaurantsForArea(Area $area)
    {
        $this->info("Retrieving restaurants for area '{$area->name}'");
        $restaurants_url = $this->country->base_url . '/restaurantes/' . $area->city->slug . '/' . $area->slug . '?page=';
        $list = $this->getDomFromUrl($restaurants_url . 1);
        if (!$list) {
            return collect([]);
        }

        $latest_page = 1;
        if (!empty($list->find('footer ul.pagination', 0))) {
            $latest_page_link = $list->find('footer ul.pagination', 0)->last_child()->prev_sibling()->find('a', 0)->href;
            $latest_page = str_replace('page=', '', parse_url($latest_page_link)['query']);
        }

        $this->debug("$latest_page pages found... scrapping.");

        $restaurants = collect([]);
        $restaurants = $restaurants->merge($this->getRestaurantsFromPage($list, $area));

        $rolling = new RollingCurlX($this->concurrent_reqs);
        for ($page = 2; $page <= $latest_page; $page++) {
            $this->info("Page $page");
            $rolling->addRequest($restaurants_url . $page, [], function ($response) use (&$restaurants, $area) {
                $dom = $this->cleanAndGetDom($response);
                $restaurants = $restaurants->merge($this->getRestaurantsFromPage($dom, $area));
            });
        }
        $rolling->execute();
        // Clean up
        $rolling = null;

        $this->info(count($restaurants) . ' restaurants found and stored.');
        return $restaurants;
    }

    private function getRestaurantsFromPage($dom, $area)
    {
        $restaurants = collect([]);
        foreach ($dom->find('#resultList li[class=peyaCard]') as $li) {
            $json_info = json_decode(html_entity_decode($li->{'data-info'}, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES), true);
            $rest_info = [
                'id' => $json_info['id'],
                'name' => $json_info['name'],
                'logo' => $li->find('figure a img', 0)->{'data-original'} ?? '',
                'url' => $li->{'data-url'},
                'address' => !empty($li->find('span.address', 0)) ? trim(html_entity_decode($li->find('span.address', 0)->plaintext)) : '',
                'categories' => $json_info['allCategories'],
                'top_categories' => json_encode($json_info['topCategories']),
                'rating' => $json_info['ratingScore'],
                'rating_count' => $json_info['validReviewsCount'],
                'delivery_time' => $json_info['deliveryTime'],
                'area_id' => $area->id,
                // coordinates
                // schedules
            ];

            $rules = [
                'id' => 'required|numeric',
                'name' => 'required|string',
                'url' => 'required|string',
            ];

            $validator = Validator::make($rest_info, $rules);
            if ($validator->fails()) {
                $this->debug('Not enough info on this restaurant!!, skipping: ' . json_encode($rest_info));
                continue;
            }

            $restaurants->push(Restaurant::updateOrCreate(['id' => $json_info['id']], $rest_info));
        }
        return $restaurants;
    }

    private function getMenuForRestaurant(Restaurant $restaurant)
    {
        $this->debug("Retrieving restaurant info ( {$restaurant->name} - {$restaurant->url} )");

        $dom = $this->getDomFromUrl($restaurant->url);
        if ($dom) {
            return $this->getMenuFromDom($dom, $restaurant);
        }
        return false;
    }

    private function getMenuFromDom($dom, $restaurant)
    {
        $this->debug("Parsing menu for - {$restaurant->name} ( {$restaurant->url} )");
        $this->updateRestaurantInfoFromMenu($dom, $restaurant);

        $menu = collect([]);
        // Go through all sections of the page
        foreach ($dom->find('#menu section[class!=mosection]') as $section) {
            $section_name = '';
            if ($section_name = $section->find('.sectionTitle h3', 0)) {
                $section_name = html_entity_decode($section->find('.sectionTitle h3', 0)->{'data-name'});
            }
            // Go through all dishes from each section
            foreach ($section->find('ul li') as $item) {
                if (isset($item->{'data-id'})) {
                    $id = $item->{'data-id'};
                    if (empty($item->find('.productName', 0))) {
                        $this->error($id . ' could not be parsed.');
                        continue;
                    }
                    $dish = [
                        'id' => $id,
                        'name' => trim(html_entity_decode($item->find('.productName', 0)->plaintext)),
                        'slug' => $item->{'data-autolink'},
                        'section' => $section_name,
                        'most_ordered' => strpos($item->class, 'most') !== false,
                        'restaurant_id' => $restaurant->id,
                    ];
                    // Find price
                    if (!empty($item->find('.productPrice span', 0))) {
                        $dish['price'] = $item->find('.productPrice span', 0)->innertext;
                    } elseif (!empty($item->find('.price span.has-discount', 0))) {
                        $dish['price'] = $item->find('.price span.has-discount', 0)->innertext;
                    } elseif (!empty($item->find('.price span', 0))) {
                        $dish['price'] = $item->find('.price span', 0)->innertext;
                    }
                    if (!isset($dish['price'])) {
                        \Log::notice('This dish have no price.', ['dish' => $dish, 'restaurant' => $restaurant]);
                        continue;
                    }
                    // Remove dots
                    $dish['price'] = str_replace('.', '', $dish['price']);
                    // Remove currency symbol
                    $dish['price'] = floor(str_replace('$', '', $dish['price']));   // It should be an integer, c'mon, half peso? really?

                    if (!empty($item->find('p', 0))) {
                        $dish['description'] = $item->find('p', 0)->innertext;
                    }

                    if (mb_strlen($dish['name']) >= 191) {
                        \Log::notice('This dish have a name that is too long (' . mb_strlen($dish['name']) . ').', ['dish' => $dish, 'restaurant' => $restaurant]);
                        continue;
                    }

                    // Add to the DB
                    $menu->push(Dish::updateOrCreate(['id' => $id], $dish));
                }
            }
        }
        $dom = null;

        $this->info($menu->count() . ' dishes found for this restaurant.');
        return $menu;
    }

    private function updateRestaurantInfoFromMenu($dom, $restaurant)
    {
        $this->debug("Update restaurant info from menu. ({$restaurant->name})");
        // Header
        $header = $dom->find('#profileHeader', 0);
        // Add coordinates and schedule time
        if (!empty($header->{'data-lat'}) && !empty($header->{'data-lng'})) {
            $restaurant->coordinates = json_encode([$header->{'data-lat'}, $header->{'data-lng'}]);
        }
        $schedules = [];
        foreach ($dom->find('ul.dateshours li') as $li) {
            if (!$li->find('div.datecol', 0)) {
                break;
            }
            $day = $li->find('div.datecol', 0)->innertext;
            $hours = [];
            $opens = $closes = false;
            foreach ($li->find('div.hourscol span') as $span) {
                if (strpos($span->itemprop, 'opens') !== false) {
                    $opens = $span->innertext;
                }
                if (strpos($span->itemprop, 'closes') !== false) {
                    $closes = $span->innertext;
                }

                if ($opens && $closes) {
                    $schedules[$day][] = [$opens, $closes];
                    $opens = $closes = false;
                }
            }
        }
        // Automatic casting by Eloquent
        $restaurant->schedules = $schedules;
        $restaurant->save();
    }

    private function getDomFromUrl($url)
    {
        $this->debug("Getting DOM from $url");

        $html = file_get_contents($url);
        if (!$html) {
            $this->error('Could not retrieve ' . $url);
            return false;
        }
        return $this->cleanAndGetDom($html);
    }

    private function cleanAndGetDom($html)
    {
        // Clean malformed attrs
        $html = str_replace('data-visa=true', '', $html);
        $html = str_replace('data-mastercard=true', '', $html);
        $html = str_replace('data-ticketalimentacion=true', '', $html);
        $html = str_replace('data-ticketrestaurant=true', '', $html);
        $html = str_replace('data-oca=true', '', $html);

        return HtmlDomParser::str_get_html($html);
    }

    private function tryToFollowRedirect(string $url)
    {
        $guzzle = resolve('GuzzleHttp\Client');
        $final_url = $url;
        try {
            // This should be head, but they ban HEAD reqs for some reason beyond my understanding
            $guzzle->get($url, [
                'on_stats' => function (\GuzzleHttp\TransferStats $stats) use (&$final_url) {
                    $final_url = $stats->getEffectiveUri();
                }, ]);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            if ($e->getResponse()->getStatusCode() > 299) {
                return $e->getResponse()->getStatusCode();
            }
            $final_url = $url;
        }

        return (string) $final_url;
    }

    private function debug(string $text)
    {
        if (config('app.debug')) {
            $this->line("[DEBUG] - $text");
        }
    }
}
